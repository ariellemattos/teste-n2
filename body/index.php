<body>
    <main class="container">
      <header class="blog-header py-3">
      </header>

      <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Teste de Desenvolvimento</h1>
          <p class="lead my-3">Teste PHP/JS/HTML para correção de itens com problemas, existe uma imagem que contém exatamente como deve ficar. Procure a imagem na pasta /img/layout/layout.jpg.  Todos os Links devem estar funcionando .</p>
        </div>
      </div>

      <div class="row mb-2">
        <?php

// Caminho do arquivo json
          $url = "json/posts.json";

// Conteudo do arquivo
          $stringJson = file_get_contents($url);

// Decodificando a string Json
          $jsonPost = json_decode($stringJson, true);

// foreach para trazer os arquivos
            foreach($jsonPost['posts'] as $noticia){

        ?>
          <div class="col-md-6">
            <div class="card flex-md-row mb-4 box-shadow h-md-250 float">
              <div class="card-body d-flex flex-column align-items-start">
                <!--  Categoria da materia -->
                <strong class="d-inline-block mb-2 text-success">
                  <?php echo $noticia["chapeu"] ?>
                </strong>

                <!--  Titulo da materia -->
                  <h3 class="mb-0">
                      <a class="text-dark" href="<?php echo $noticia["link"]?>">
                        <?php echo $noticia["titulo"]?>
                      </a>
                  </h3>

                  <!--  Titulo da materia -->
                <p class="card-text mb-auto">
                  <?php echo $noticia["linhafina"] ?>
                </p>
              </div>
            </div>
        </div>
        <?php
          }
        ?>
      </div>

    </main>

    <?php
      include 'footer/index.php';
      ?>
</body>
